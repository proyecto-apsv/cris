<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<%@ include file = "Header.jsp"%>
<title>Publications</title>
</head>
<body>

<table>
	<tr>
	    <td>${pub.id}</td>
		<td>${pub.title}</td>
		<td>${pub.publicationName}</td>
		<td>${pub.publicationDate}</td>
		<td>${pub.authors}</td>
		<td>${pub.citeCount}</td>
	</tr>
</table>

<c:if test="${user.id == ri.id}">
<h2>Create publication</h2>
<form action="CreatePublicationServlet" method="post">
	<input type="text" name="id" placeholder="Publication Id"> 
	<input type="text" name="title" placeholder="Title">
	<input type="text" name="publicationname" placeholder="Publication name">
	<input type="text" name="publicationdate" placeholder="Publication date">
	<input type="text" name="authors" placeholder="Authors">
	<button type="submit">Create publication</button>
</form>
</c:if>

<c:if test="${user.id == 'root'}">
<h2>Update publications of researcher</h2>
<a href="UpdatePublicationsQueueServlet?id=${ri.id}"> ${ri.id}</a>
</c:if>

</body>
</html>

